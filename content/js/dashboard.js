/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.86531986531986, "KoPercent": 0.13468013468013468};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.07508417508417509, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.27419354838709675, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.014705882352941176, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.03125, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.037037037037037035, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.1, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.11290322580645161, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.02127659574468085, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.0, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.0, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.07142857142857142, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.1984126984126984, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.32857142857142857, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.0, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.0, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.0, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.0, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [0.125, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [0.34782608695652173, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.0, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.0, 500, 1500, "portfolioByID"], "isController": false}, {"data": [0.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.27941176470588236, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "getPastEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.10344827586206896, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.0, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.0, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.9242424242424242, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.0, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [0.0, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1485, 2, 0.13468013468013468, 84613.86936026941, 9, 404144, 60778.0, 214251.60000000003, 261156.60000000003, 375844.8600000002, 3.2427758160985807, 159.0221485273212, 5.829251202665846], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllChildActiveSubsidies", 40, 0, 0.0, 65026.52500000001, 3385, 213419, 63657.0, 131313.7, 168196.7999999999, 213419.0, 0.10724378120123759, 0.12200289239829266, 0.17479479572740775], "isController": false}, {"data": ["getUpcomingEvents", 31, 0, 0.0, 6630.1612903225805, 115, 65733, 2836.0, 13180.000000000002, 43237.199999999946, 65733.0, 0.10187783861235811, 0.05939557583161894, 0.14625041285172502], "isController": false}, {"data": ["findAllFeeGroup", 34, 0, 0.0, 44949.970588235294, 1470, 279744, 33307.5, 124171.0, 202056.75, 279744.0, 0.09737265661246255, 0.0893850558747215, 0.1048848049253381], "isController": false}, {"data": ["getListDomainByLevel", 32, 0, 0.0, 65130.78125, 196, 204035, 62156.0, 122314.3, 191344.39999999997, 204035.0, 0.09860079312012966, 0.5198778573046241, 0.17534379323413682], "isController": false}, {"data": ["getPendingEvents", 27, 0, 0.0, 106825.70370370371, 279, 236938, 99605.0, 227153.6, 233530.8, 236938.0, 0.07157380186781113, 0.03571700464301904, 0.0931018594608637], "isController": false}, {"data": ["getPortfolioTermByYear", 55, 0, 0.0, 23382.527272727268, 137, 149627, 9834.0, 64226.6, 104844.79999999981, 149627.0, 0.16319845228522342, 0.16325350858127136, 0.20711888803250914], "isController": false}, {"data": ["findAllCurrentFeeTier", 48, 0, 0.0, 185567.37499999997, 76657, 370255, 168493.5, 280386.9000000001, 321082.6, 370255.0, 0.1236699044134697, 1.236407179939711, 0.31303944554659524], "isController": false}, {"data": ["findAllClassResources", 31, 0, 0.0, 7868.290322580645, 113, 33085, 4202.0, 17696.8, 29795.199999999993, 33085.0, 0.10220970794400228, 0.05998831491635289, 0.14193574677380003], "isController": false}, {"data": ["listBulkInvoiceRequest", 47, 0, 0.0, 112189.70212765956, 263, 342692, 104242.0, 215025.0, 234161.59999999998, 342692.0, 0.12213915027533281, 0.19588756506508456, 0.18499787312211055], "isController": false}, {"data": ["findAllCustomSubsidies", 30, 0, 0.0, 59376.26666666665, 4253, 176043, 50898.5, 128764.5, 150481.19999999995, 176043.0, 0.082167018158911, 0.0974128516063652, 0.10270877269863876], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 33, 0, 0.0, 48712.87878787879, 4897, 133145, 40321.0, 95309.4, 109739.0999999999, 133145.0, 0.08857752546603857, 7.430687666921663, 0.12758969732656922], "isController": false}, {"data": ["getAllChildDiscounts", 30, 0, 0.0, 69740.56666666668, 3320, 280200, 64133.0, 100461.6, 202992.6499999999, 280200.0, 0.08007003459025494, 0.04894906411474569, 0.13191225425171882], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 27, 0, 0.0, 135655.8148148148, 53460, 255270, 134004.0, 230743.6, 246445.99999999994, 255270.0, 0.07306697264589039, 0.05258824105470822, 0.09582904713225664], "isController": false}, {"data": ["getMyDownloadPortfolio", 42, 0, 0.0, 18455.666666666668, 17, 151240, 6453.5, 51311.50000000003, 79336.15000000004, 151240.0, 0.11329396817518483, 0.06660446175923954, 0.11628121147667898], "isController": false}, {"data": ["getListDomain", 63, 0, 0.0, 52401.63492063492, 60, 244315, 6632.0, 146190.4, 174177.19999999995, 244315.0, 0.16236778623121173, 0.6712445160602359, 0.2688989941895528], "isController": false}, {"data": ["getLessonPlan", 35, 0, 0.0, 8673.085714285713, 28, 81719, 2626.0, 33464.39999999997, 52682.199999999844, 81719.0, 0.11479625305030045, 0.0747745124849125, 0.1840775854576107], "isController": false}, {"data": ["getAdvancePaymentReceipts", 28, 0, 0.0, 68811.35714285714, 3291, 214536, 62052.0, 141510.2, 184036.3499999998, 214536.0, 0.07488833614164593, 0.11262503677552221, 0.11942641886651155], "isController": false}, {"data": ["findAllProgramBillingUpload", 35, 0, 0.0, 53117.37142857143, 3419, 182559, 53642.0, 95482.0, 114688.59999999964, 182559.0, 0.09949852742179416, 0.13654951366541204, 0.1483732923565231], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 33, 0, 0.0, 107324.78787878789, 10815, 379143, 92539.0, 184588.80000000002, 272281.69999999955, 379143.0, 0.08530857144334349, 0.12335055957898927, 0.15337214846405794], "isController": false}, {"data": ["getChildSemesterEvaluation", 26, 0, 0.0, 58799.1923076923, 2008, 265269, 41148.0, 159046.2, 228572.19999999984, 265269.0, 0.07723039907323521, 0.059506625848420494, 0.11840988920408134], "isController": false}, {"data": ["getCentreManagementConfig", 37, 0, 0.0, 31086.567567567567, 2053, 145712, 18839.0, 72453.8, 106450.40000000007, 145712.0, 0.09850905218317359, 0.11620989749733758, 0.15950000832002129], "isController": false}, {"data": ["updateClassResourceOrder", 36, 0, 0.0, 16100.416666666668, 9, 71307, 9416.0, 53164.3, 57242.049999999974, 71307.0, 0.11237007210412961, 0.0662807847176702, 0.10688325217717015], "isController": false}, {"data": ["getChildPortfolio", 41, 0, 0.0, 222784.12195121954, 115846, 360355, 226322.0, 283506.20000000007, 336450.8, 360355.0, 0.09934408672496468, 0.20175245013411452, 0.2403079129079468], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 46, 0, 0.0, 7256.152173913044, 20, 56067, 2236.5, 22921.600000000013, 38989.49999999999, 56067.0, 0.1493079899249565, 0.08938066193749837, 0.2725162433298278], "isController": false}, {"data": ["findAllCentreForSchool", 64, 1, 1.5625, 197027.32812499997, 41739, 351308, 193415.0, 306740.0, 331729.0, 351308.0, 0.16639670534523418, 150.047002702159, 0.4092379908377814], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 35, 0, 0.0, 92632.85714285716, 13429, 229488, 82615.0, 173625.79999999996, 226163.99999999997, 229488.0, 0.09149298248824315, 0.057272462670863146, 0.12178216321433145], "isController": false}, {"data": ["portfolioByID", 38, 0, 0.0, 82378.68421052632, 17217, 211612, 80725.5, 137560.80000000002, 163280.74999999985, 211612.0, 0.09896477635262713, 0.16334071228595612, 0.34782639657529785], "isController": false}, {"data": ["invoicesByFkChild", 47, 0, 0.0, 120123.1489361702, 3327, 320160, 109625.0, 221624.40000000008, 285536.19999999984, 320160.0, 0.12117149633907394, 0.34736852812081054, 0.35215466123543365], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 34, 0, 0.0, 6898.470588235295, 13, 35427, 2762.5, 25963.0, 29722.5, 35427.0, 0.12452844009815771, 0.07515485935611473, 0.13608137155257666], "isController": false}, {"data": ["findAllFeeDraft", 36, 0, 0.0, 86814.55555555558, 21681, 295519, 74625.0, 144550.20000000007, 195957.64999999985, 295519.0, 0.09510173243655923, 0.05405196120906003, 0.1514755132851837], "isController": false}, {"data": ["getPastEvents", 28, 0, 0.0, 49215.28571428572, 4898, 168444, 38714.0, 101597.50000000003, 146898.44999999987, 168444.0, 0.07605265016324159, 0.037729244416920625, 0.09580851436580239], "isController": false}, {"data": ["findAllConsolidatedRefund", 33, 0, 0.0, 127079.93939393938, 58762, 241548, 118773.0, 202331.0, 227867.19999999995, 241548.0, 0.082475463549594, 0.12346668662947773, 0.14247958497971852], "isController": false}, {"data": ["getMyDownloadAlbum", 29, 0, 0.0, 17126.48275862069, 463, 63089, 9090.0, 41404.0, 53372.0, 63089.0, 0.101548794195611, 0.06644305870220642, 0.16035585958428025], "isController": false}, {"data": ["getRefundChildBalance", 34, 0, 0.0, 60192.79411764706, 2057, 240812, 55913.5, 97958.5, 179924.0, 240812.0, 0.09135389154143571, 0.08716089066013935, 0.1430080938876186], "isController": false}, {"data": ["findAllUploadedGiroFiles", 35, 0, 0.0, 69772.85714285714, 12156, 206187, 56924.0, 136056.59999999998, 168927.7999999998, 206187.0, 0.0939125861647978, 26.87315990026483, 0.13435736204241092], "isController": false}, {"data": ["findAllInvoice", 67, 1, 1.492537313432836, 199779.9850746269, 2159, 404144, 178736.0, 383943.0, 387396.8, 404144.0, 0.14630705702262956, 0.2998305185602512, 0.47034153280662794], "isController": false}, {"data": ["getAllArea", 33, 0, 0.0, 334.63636363636374, 51, 3791, 119.0, 494.2000000000005, 3261.0999999999976, 3791.0, 0.11111709721735852, 0.08768819932218572, 0.1044978170120276], "isController": false}, {"data": ["getChildChecklist", 26, 0, 0.0, 150605.23076923078, 61349, 316396, 132295.5, 259819.9, 298069.99999999994, 316396.0, 0.06796941376380629, 0.16795266649238613, 0.223821155480034], "isController": false}, {"data": ["bankAccountInfoByIDChild", 31, 0, 0.0, 159535.51612903224, 3710, 345618, 147384.0, 261241.4, 310901.3999999999, 345618.0, 0.07991565032004928, 0.12186733873150662, 0.18394647246519158], "isController": false}, {"data": ["findAllCreditDebitNotes", 28, 0, 0.0, 223492.1428571428, 125833, 316052, 220069.0, 298645.5, 313765.55, 316052.0, 0.07053429730206313, 0.14455201274655516, 0.16476371010403806], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 2, 100.0, 0.13468013468013468], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1485, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllCentreForSchool", 64, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllInvoice", 67, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
